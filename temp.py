import unidecode
from requests import *
from sys import exit
import bs4
import os


# Ta funkcja rysuje ########## i nowa linia

def pbr():
    print('\n' + '################'  '####################' + '\n')

# Ta funkcja bierze plik TXT z nazwami artystów, odzielonymi '\n' i przerabia to na listę.


def open_txt_make_pylist_by_breakline(url):
    with open(url, 'r') as file:
        data = file.read().replace('\n', ',')
        print(type(data))
        x = data.split(",")
        # print(type(x))
        # print(x)
        return (x)


# Wyrzucamy  poslkie znaki, spacje na podlogi i lowercase, robiac format z url-i tekstowo.pl


url = "artists_list_pop.txt"
x = open_txt_make_pylist_by_breakline(url)
lista = []
for i in x:
    # print(type(i))
    i = i.replace(" ", "_")
    i = i.replace(".", "_")
    i = i.lower()
    i = unidecode.unidecode(i)
    # print(i)
    lista.append(i)
print(type(lista))
print(lista)

pbr()

# Usuwam dwa rekordy które nie wiem czemu się źle zapisały...

#a = lista.index('letni')
#a = lista.pop(269)
#a = lista.pop(269)

# Tworze adresy url do strony ze wszystkimi dostepnymi piosenkami dla kazdego artysty z listy

songs_urls = []
n = 0
for i in lista:
    print(
        i)  # i to nazwa artysty # m, n to numer strony # chcę żeby dla każdego i, pętla przerzucania strony wykonywała się tak długo aż nie będzie nowych elementów podczas okrążenia.
    while True:
        q = len(songs_urls)
        n = n + 1
        m = str(n)
        url1 = 'https://www.tekstowo.pl/piosenki_artysty,' + i + ',najlepsze,malejaco,strona,' + m + '.html'
        # dodaj kolejną liczbę naturalną w url
        response = get(url1)
        print(url1)
        print(str(type(url1)))
        # Jeśli odpowiedź jest OK to parsujemy stronę o numerze n
        if response.status_code == 200:
            print('OK')
            data = get(url1)
            print(data)
            print(type(data))
            # open(str(i)+ "_indeks_tesktow.html", "w+").write(data.text)
            soup = bs4.BeautifulSoup(data.text, features="html.parser")
            pbr()
            # print(soup)  # W tej zupie nie ma wszystkiego! Jest jakby ucięta :|
            # print(tada)
            # listaBrudMinus = soup.find_all("a", {'class': 'title', 'rel': 'loginbox'})
            # listaBrud = soup.find_all("a", {'class': 'title'})
        else:
            print('nie ma takiego artysty')

        # Robimy zupe i wyciągamy z niej linki do piosenek, zapisujemy te linki do songs_urls[] liczymy okrążenia będzie ich maksymalnie 30 bo tyle jest piosenek na stronie
        licznik = 0

        # znacznik linka ktory nas interesuje jest zatytuowany a, i ma atrybut class=title
        for a in soup.find_all("a", {'class': 'title'}):
            # są też takie linki ktore spelniaja ten warunek, ale maja te atrybut rel = 'loginbox' co wykorzystamy by ich nie dodawać do nasej listy songs_urls[]
            if 'rel="loginbox' in str(a):
                continue
            # kolejne wykluczenie czegoś co pasowało to find.all("a",{'class':'title'}) ale nie było tym o co chodzi, tutaj wykluczamy na podstawie tego w czym zawarty jest niechciany link, jest zawarty w boksie
            opa = a.parent.parent['class']
            if len(opa) == 2:
                continue
            # print(type(a))
            a = a.get('href')
            # print(a)
            songs_urls.append(a)
            # print('\n' + '\n')
            licznik += 1
            #print(licznik)
        #pbr()
        #print(len(songs_urls))
        #print(q)
        r = len(songs_urls)
        # print(songs_urls)
        pbr()
        if r == q:  # jeśli piosenki tego artysty się skończyły to parsujemy wszystkie teksty do folderu, zerujemy n i lecimy następnego gościa z listy
            for l in songs_urls:
                url = 'https://www.tekstowo.pl' + l
                p = get(url)

                # requesty zle okreslily kodowanie strony trzeba było je zmienic
                #print(p.encoding)  # sprawdza jaki jest encoding
                p.encoding = 'utf-8'  # nadaje encoding
                #print(p.encoding)

                p = p.text
                zupa = bs4.BeautifulSoup(p, features="html.parser")
                tekst = zupa.find('div', attrs={'class': 'song-text'})
                tekst = tekst.get_text()
                tekst = tekst[54:]
                tekst = tekst[:-37]
                #print(tekst)
                pbr()
                # dobra mam już tekst każdej piosnki teraz chcę go wyeksportowac do pliku txt w formacie "nazwa artysty - nazwa piosenki.txt")
                l = l[:-5]
                filename = l + '.txt'
                filename = filename[10:]
                print(filename)
                #print(type(filename))
                os.chdir('/Users/apple/PycharmProjects/Rymer/output')
                s = open(filename, "w+").write(tekst)

            songs_urls = []
            n = 0
            break
